package hu.dpc.edu.state;

import hu.dpc.edu.Calculator;
import hu.dpc.edu.CalculatorState;
import hu.dpc.edu.Operator;

public class InitialState extends CalculatorState {

    @Override
    public void operatorPressed(Calculator calculator, Operator operator) {
        calculator.setOperator(operator);
    }

    @Override
    protected void numberButtonPressed(Calculator calculator, String num) {
        calculator.setDisplayedText(num);
        calculator.setState(new EnteringFirstNumberState());
    }

    @Override
    public void calculateButtonPressed(Calculator calculator) {
        // do nothing
    }

    @Override
    public void decimalSeparatorPressed(Calculator calculator) {
        calculator.setDisplayedText("0.");
        calculator.setState(new EnteringFirstNumberDecimalPartState());
    }

    @Override
    public void onEnter(Calculator calculator) {
        calculator.setDisplayedText("0");
    }

    @Override
    public String toString() {
        return "Initial";
    }
}
