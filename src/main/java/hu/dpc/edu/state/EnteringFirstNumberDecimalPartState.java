package hu.dpc.edu.state;

import hu.dpc.edu.Calculator;

public class EnteringFirstNumberDecimalPartState extends EnteringFirstNumberState {
    @Override
    public void decimalSeparatorPressed(Calculator calculator) {
        //do nothing
    }

    @Override
    public String toString() {
        return "Entering first number decimal part";
    }
}
